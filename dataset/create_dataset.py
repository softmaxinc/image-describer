from scrapers import web_scraper as scraper
import json
import urllib
import os

PROJECT_PATH = os.path.dirname(os.path.abspath(__file__)).replace('\ImageDescriber\dataset','')\

class DatasetCreator(object):
    """
    Creates a dataset for the COCOD challenge
    """

    def __init__(self):
        self.init_objects()

    def getFileInfo(self, url):
        from urlparse import urlparse
        from os.path import splitext, basename

        disassembled = urlparse(url)
        filename, file_ext = splitext(basename(disassembled.path))
        return filename, file_ext

    def save_data(self):
        """ Saves image urls, labels and images onto file """
        with open('{0}/train_image_labels.json'.format(PROJECT_PATH), 'w') as outfile:
            json.dump(self.train_image_labels, outfile)

        with open('{0}/test_image_labels.json'.format(PROJECT_PATH), 'w') as outfile:
            json.dump(self.test_image_labels, outfile)

        # Download all testing files
        for query, images in self.test_image_labels.iteritems():
            index = 0
            for image_url in images:
                filename, file_ext = self.getFileInfo(image_url)
                if file_ext == "": file_ext = ".jpg"
                image_path = str.replace(query, " ", "-")
                image_path = image_path + str(index) + file_ext
                image_path = str.format("{0}/data/test/{1}", PROJECT_PATH, image_path)
                try:
                    urllib.urlretrieve(image_url, image_path)
                except Exception, e:
                    print("Error occured " + e)
                index += 1

        # Download all training files
        for query, images in self.train_image_labels.iteritems():
            index = 0
            for image_url in images:
                filename, file_ext = self.getFileInfo(image_url)
                image_path = str.replace(query, " ", "-")
                image_path = image_path + str(index) + file_ext
                image_path = str.format("{0}/data/train/{1}", PROJECT_PATH, image_path)
                try:
                    urllib.urlretrieve(image_url, image_path)
                except Exception, e:
                    print("Error occured " + e)
                index += 1

    def generate_dataset(self):
        train_image_labels = {}
        test_image_labels = {}

        print("Creating train set")

        currIndex = 0
        for train_query in self.train_queries:
            currIndex+=1
            print("{0}".format(currIndex))

            # Unpack the training query and get images from the web
            object = train_query[0]
            action = train_query[1]
            location = train_query[2]

            query = self.scaffold_query(object, action, location)
            images = self.get_images(object, action, location)
            train_image_labels[query] = images

        print("Creating test set")

        for test_query in self.test_queries:
            currIndex+=1
            print("{0}".format(currIndex))

            # Unpack the training query and get images from the web
            object = test_query[0]
            action = test_query[1]
            location = test_query[2]

            query = self.scaffold_query(object, action, location)
            images = self.get_images(object, action, location)
            test_image_labels[query] = images

        self.train_image_labels = train_image_labels
        self.test_image_labels = test_image_labels


    def init_objects(self):
        self.animal_list()
        self.generate_queries()

    def generate_queries(self):
        """ Generates queries based on object/action/location dataset
        :Creates list of queries to use to build the dataset
        """
        train_queries = []
        test_queries = []

        # For training: give distinct actions for each group of objects
        for wild_animal in self.wild_animals:
            for wild_action in self.wild_actions:
                train_queries.append([wild_animal, wild_action, ""])

        for domesticated_animal in self.domesticated_animals:
            for domesticated_action in self.domesticated_actions:
                train_queries.append([domesticated_animal, domesticated_action, ""])

        # For testing see if can learn when first group does second group's action, etc.
        for wild_animal in self.wild_animals:
            for domesticated_action in self.domesticated_actions:
                test_queries.append([wild_animal, domesticated_action, ""])

        for domesticated_animal in self.domesticated_animals:
            for wild_action in self.wild_actions:
                test_queries.append([domesticated_animal, wild_action, ""])

        self.train_queries = train_queries
        self.test_queries = test_queries




    def animal_list(self):
        """Scaffolds list of images/actions/locations for the CODO dataset"""
        self.wild_animals = ["monkey", "dog", "llama"]

        self.domesticated_animals = ["people", "cat", "horse",  "llama"]

        self.wild_actions = ["reading a book", "smiling", "running", "fighting"]

        self.domesticated_actions = ["drinking water", "playing together", "eating grass",
                                     "playing piano"]

    def scaffold_query(self, object, action, location):
        query = object + " " + action + " " + location
        return query

    def get_images(self, object, action, location):
        """ Get images from web describing object w/action and location
        :param object: Object/noun to search for
        :param action: What the object is doing
        :param location: Where it is doing it
        :return: Images w/object's action and location
        """

        query = self.scaffold_query(object, action, location)
        images = scraper.crawl_bing(query)[0:20]

        for i in range(0, len(images)):
            image = images[i]
            image = str.replace(image, "w=154&h=164", "w=250&h=250")
            images[i] = image

        # If you're being throttled:
        if len(images) == 0:
            print("Crawling google instead")
            images = scraper.crawl_google(query)
        return images




