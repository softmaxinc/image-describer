import json
import os
import requests
import urllib
import mechanize
from bs4 import BeautifulSoup
from urlparse import urlparse
import hashlib


def crawl_instagram(username, items=[], max_id=None):
    url   = 'http://instagram.com/' + username + '/media' + ('?&max_id=' + max_id if max_id is not None else '')
    media = json.loads(requests.get(url).text)

    # Get the image urls
    imageUrls = [ curr_item[ curr_item['type'] + 's' ]['standard_resolution']['url'] for curr_item in media['items']]

    # Get the caption
    captions = [ curr_item[ curr_item['type'] + 's' ]['standard_resolution']['url'] for curr_item in media['items']]
    items.extend(imageUrls)

    if 'more_available' not in media or media['more_available'] is False:
        return items
    else:
        max_id = media['items'][-1]['id']
        return crawl_instagram(username, items, max_id)

def crawl_bing(search):
    search = search.replace(" ","%20")
    try:
        browser = mechanize.Browser()
        browser.set_handle_robots(False)
        browser.addheaders = [('User-agent','Mozilla')]

        url = "http://www.bing.com/images/search?q=" + search
        htmltext = browser.open(url)
        img_urls = []
        formatted_images = []
        soup = BeautifulSoup(htmltext)
        results = soup.findAll("img")
        for r in results:
            try:
                if "http" in r['src2'] and "?id=" in r['src2']:
                    img_urls.append(r['src2'])
            except:
                a=0

        return img_urls

    except:
        return []

def crawl_google(term):
    img_list = getPics(term)
    if len(img_list) > 0:
        for img in img_list:
            a = 5
            # print("Image url is "  + img)
            # savePic(img)
    return img_list

def getPics(search):
    search = search.replace(" ","%20")
    try:
        browser = mechanize.Browser()
        browser.set_handle_robots(False)
        browser.addheaders = [('User-agent','Mozilla')]

        url = "https://www.google.com/search?site=imghp&tbm=isch&source=hp&biw=1414&bih=709&q="+search+"&oq="+search
        htmltext = browser.open(url)
        img_urls = []
        formatted_images = []
        soup = BeautifulSoup(htmltext)
        results = soup.findAll("img")
        for r in results:
            try:
                if "http" in r['src']:
                    img_urls.append(r['src'])
            except:
                a=0

        return img_urls

    except:
        return []

def savePic(url):
    hs = hashlib.sha224(url).hexdigest()
    file_extension = url.split(".")[-1]
    uri = ""
    dest = uri+hs+"."+file_extension
    print dest
    try:
        urllib.urlretrieve(url,dest)
    except:
        print "save failed"