class ImageCaption(object):

    """An ImageCaption stores image/annotation captions"""
    def __init__(self, imagePath, imageName,
                 features = None, classes = None, categories = None):
        self.imagePath = imagePath
        self.imageName = imageName
        self.features = features
        self.classes = classes
        self.categories = categories
        self.captions = []

    def addCaption(self, caption):
        """ Adds a caption to the list of captions for this image"""
        self.captions.extend(caption)

    def __str__(self):
        return "Image Name:{0} Path:{1} Description{2}".format(self.imageName, self.imagePath,
                                                               self.captions[0]['text'])

