from flask import Flask
import flask
from softmax_wrapper import SoftmaxWrapper
from scrapers import web_scraper as scraper
from dataset.create_dataset import DatasetCreator

app = Flask(__name__)



@app.route('/')
def hello_world():
    wrapper = SoftmaxWrapper()

    username = flask.request.args.get('username', 'food')
    # pictures = scraper.crawl_instagram(username)


    googlePics = scraper.crawl_google("dogs sprinting outside")
    descriptions = []
    for i in range(0, 1):
        results = wrapper.getImageDescriptions(googlePics[0:20])
        descriptions.extend(results)

    return flask.render_template('index.html', title = 'Classify', descriptions = descriptions)


if __name__ == '__main__':
    print("Creating dataset")
    creator = DatasetCreator()
    creator.generate_dataset()

    print("Saving dataset")
    creator.save_data()

    app.run(debug=True)

