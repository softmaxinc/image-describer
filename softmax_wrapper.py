from image_annotation import ImageCaption
import json
import urllib2

class SoftmaxWrapper(object):
    """SoftmaxWrapper abstracts SoftmaxVision's REST API
    to get deep image features"""

    def __init__(self):
        self.extractionPath = "http://softmax.elasticbeanstalk.com/api/extract"
        self.classificationPath = "http://softmax.elasticbeanstalk.com/api/classify"
        self.descriptionPath = "http://softmax.elasticbeanstalk.com/api/describe"

    def getImageFeatures(self, imageUrls):
        """ Returns image features from specified imageUrl """

        # Get raw json data
        extractionData = self.getJsonData(self.extractionPath, imageUrls)
        classificationData = self.getJsonData(self.classificationPath, imageUrls)

        # Parse it into readable class
        imageAnnotations = self.getImageInfo(extractionData, classificationData)
        return imageAnnotations

    def getImageDescriptions(self, imageUrls):
        """ Returns image descriptions from imageurls
        :param imageUrls: Image urls to describe
        :return: Natural language descriptions of images
        """
        # Get raw json data
        descriptionData = self.getJsonData(self.descriptionPath, imageUrls)
        imageAnnotations = self.getImageCaptions(descriptionData)
        return imageAnnotations

    def getJsonData(self, endpoint, imageUrls):
        """Extracts features from specified image urls"""
        # Create url string
        urlString = self.createUrlString(imageUrls)

        # Make a request to base endpoint
        req = urllib2.Request(endpoint)
        req.add_header('Content-Type', 'application/json')

        # Attach POST data
        jsonData = json.dumps({'imageurls' : urlString})

        # Wait for response
        response = urllib2.urlopen(req, jsonData)
        data = json.load(response)

        # Return json data
        return data

    def createUrlString(self, strings):
        # Creates a CSV string from an array of urls
        urlString = str.join(',', strings)
        return urlString

    def getImageInfo(self, extractedData, classificationData):
        imageCaptions = []
        """ Converts softmax's JSON data to image caption class """
        # Go through all of the batches
        currFeatureData = extractedData['Result']
        currClassificationData = classificationData['Result']

        for j in range(0, len(currFeatureData)):
            # Read in all of the data
            currImage = currFeatureData[j][0]
            currClassification = currClassificationData[j][0]

            # Extract classes, features, and categories
            currFeatures = currImage['Features']
            currUrl = currImage['ImageUrl']
            currCategories = currClassification['Categories']
            currClasses = currClassification['Classes']

            imageName, imageExt = self.getFileInfo(currUrl)

            # Get image details
            imageCaption = ImageCaption(currUrl, imageName + imageExt, currFeatures, currClasses, currCategories)
            imageCaptions.append(imageCaption)

        return imageCaptions

    def getImageCaptions(self, descriptionData):
        imageCaptions = []
        """ Converts softmax's JSON data to image caption class """
        # Go through all of the batches
        descrData = descriptionData['Result']

        for j in range(0, len(descrData)):
            # Read in all of the data
            currImage = descrData[j][0]

            # Extract descriptions, url etc
            currUrl = currImage['ImageUrl']
            imageName, imageExt = self.getFileInfo(currUrl)
            descriptions = currImage['Descriptions']

            # Get image details
            imageCaption = ImageCaption(currUrl, imageName + imageExt)
            imageCaption.addCaption(descriptions)

            imageCaptions.append(imageCaption)

        return imageCaptions

    def getImageInfo(self, extractedData, classificationData):
        imageCaptions = []
        """ Converts softmax's JSON data to image caption class """
        # Go through all of the batches
        currFeatureData = extractedData['Result']
        currClassificationData = classificationData['Result']

        for j in range(0, len(currFeatureData)):
            # Read in all of the data
            currImage = currFeatureData[j][0]
            currClassification = currClassificationData[j][0]

            # Extract classes, features, and categories
            currFeatures = currImage['Features']
            currUrl = currImage['ImageUrl']
            currCategories = currClassification['Categories']
            currClasses = currClassification['Classes']

            imageName, imageExt = self.getFileInfo(currUrl)

            # Get image details
            imageCaption = ImageCaption(currUrl, imageName + imageExt, currFeatures, currClasses, currCategories)
            imageCaptions.append(imageCaption)

        return imageCaptions


    def getFileInfo(self, url):
        from urlparse import urlparse
        from os.path import splitext, basename

        disassembled = urlparse(url)
        filename, file_ext = splitext(basename(disassembled.path))
        return filename, file_ext